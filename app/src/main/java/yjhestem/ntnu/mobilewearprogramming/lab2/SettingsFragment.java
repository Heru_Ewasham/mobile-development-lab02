package yjhestem.ntnu.mobilewearprogramming.lab2;

import android.app.Fragment;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.util.Log;

/**
 * Created by Bruker on 15.02.2018.
 */

public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("SettingsFragment", "onCreate: AddPreferencesFromResource(..)");
        addPreferencesFromResource(R.xml.preferences);
        Log.d("SettingsFragment", "onCreate: Added PreferencesFromResource(..)");
    }
}
