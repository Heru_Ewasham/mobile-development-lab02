package yjhestem.ntnu.mobilewearprogramming.lab2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.prefs.Preferences;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import static android.app.Service.START_STICKY;
import static android.content.Intent.URI_ANDROID_APP_SCHEME;
import static android.content.Intent.URI_INTENT_SCHEME;
import static android.provider.AlarmClock.EXTRA_MESSAGE;


public class ListActivity extends AppCompatActivity {
    private static final String ACTION_FETCH_WEBSITE = "yjhestem.ntnu.mobilewearprogramming.lab2.action.FETCH_WEBSITE";
    private static final String ACTION_BAZ = "yjhestem.ntnu.mobilewearprogramming.lab2.action.BAZ";

    private static final String EXTRA_PARAM1 = "yjhestem.ntnu.mobilewearprogramming.lab2.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "yjhestem.ntnu.mobilewearprogramming.lab2.extra.PARAM2";
    //public static Intent getWebsiteIntentService = null;

    String TAG = "ListActivity";
    String URL = "http://www.nrk.no/toppsaker.rss";
    // AlarmManager is gotten from http://navinsandroidtutorial.blogspot.no/2014/04/android-alarm-manager-service-and.html
    GregorianCalendar calendar;
    private PendingIntent pendingIntent;
    AlarmManager alarmManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
        //pref.

        updateRSS();

        ListView lv = ( ListView ) findViewById(R.id.List);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              //  int position, long id) {
               showWeb(view, position);
            }
        });

        //startAlarmManagerWebsite();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences.Editor editor = prefs.edit();
        final String rss_url = prefs.getString("RSS_URL", URL);
        prefs.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
    }

    // Gotten from: https://coderwall.com/p/qfoxfg/schedule-a-service-using-alarmmanager
    private void startAlarmManagerWebsite() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences.Editor editor = prefs.edit();
        final String rss_url = prefs.getString("RSS_URL", URL);
        String time_interval = prefs.getString("fetchTime", "10");
        Log.d(TAG, "startAlarmManagerWebsite: Starting Alarmmanager");

        //Check and give correct value for every value in sharedPreferences.
        if(time_interval.equals("10 min")) {
            Log.d(TAG, "startAlarmManagerWebsite: FetchTime is " + time_interval + ", so we need to change it to '10'.");
            time_interval = "10";
            Log.d(TAG, "startAlarmManagerWebsite: FetchTime is now " + time_interval);
        }
        else if(time_interval.equals("60 min")) {
            Log.d(TAG, "startAlarmManagerWebsite: FetchTime is " + time_interval + ", so we need to change it to '60'.");
            time_interval = "60";
            Log.d(TAG, "startAlarmManagerWebsite: FetchTime is now " + time_interval);
        }
        else if(time_interval.equals("Once a day")) {
            Log.d(TAG, "startAlarmManagerWebsite: FetchTime is " + time_interval + ", so we need to change it to '1440'.");
            time_interval = "1440";                 // Number of minutes in a day.
            Log.d(TAG, "startAlarmManagerWebsite: FetchTime is now " + time_interval);
        }

        Log.d(TAG, "startAlarmManagerWebsite: FetchTime is " + time_interval + "min.");
        Context ctx = getApplicationContext();
/** this gives us the time for the first trigger.  */
        Calendar cal = Calendar.getInstance();
        AlarmManager am = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        long interval = 1000 * 60 * Integer.parseInt(time_interval); // time_interval minutes in milliseconds
        Intent serviceIntent = new Intent(ctx, GetWebsiteIntentService.class);
        serviceIntent.setAction(ACTION_FETCH_WEBSITE);
        serviceIntent.putExtra(EXTRA_PARAM1, rss_url);
        serviceIntent.putExtra(EXTRA_PARAM2, "");
// make sure you **don't** use *PendingIntent.getBroadcast*, it wouldn't work
        PendingIntent servicePendingIntent =
                PendingIntent.getService(ctx,
                        GetWebsiteIntentService.SERVICE_ID, // integer constant used to identify the service
                        serviceIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);  // FLAG to avoid creating a second service if there's already one running
// there are other options like setInexactRepeating, check the docs
        am.setRepeating(
                AlarmManager.RTC_WAKEUP,//type of alarm. This one will wake up the device when it goes off, but there are others, check the docs
                cal.getTimeInMillis(),
                interval,
                servicePendingIntent
        );
        Log.d(TAG, "startAlarmManagerWebsite: Started Alarmmanager");
    }

    SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals("xmlData")){
                updateRSS();
            }
            if (key.equals("fetchTime")) {      // When delay changes.
                startAlarmManagerWebsite();
            }
            if (key.equals("RSS_URL")) {
                startAlarmManagerWebsite();
            }
        }
    };

    public void goToPreferences(View view) {
        Intent prefIntent = new Intent(this, SettingsActivity.class);
        startActivity(prefIntent);
    }

    public void showWeb(View view, int position) {
        TextView textView = (TextView) view.findViewById(R.id.link);
        String url = textView.getText().toString();
        Intent intent = new Intent(this, WebActivity.class);
        Log.d("lab2", "showWeb: url to send: " + url);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    public void updateRSS() {
        Log.d("lab2", "onCreate: Starting GetData(..)");

        /** Getting the latest news from SharedPreferences */
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String xmlContent = prefs.getString("xmlData", "null");
        final int displayNumber = Integer.parseInt(prefs.getString("displayNumber", "10"));

        /** The parsing of the xml data is done in a non-ui thread */
        ListViewLoaderTask listViewLoaderTask = new ListViewLoaderTask(this, displayNumber);

        Log.d(TAG, "updateRSS: Data in xmlData: " + xmlContent);

        if (xmlContent == "null") {
            Log.d(TAG, "updateRSS: Didn't find XML-content saved, get a new copy");
            final String rss_url = prefs.getString("RSS_URL", URL);
            new GetData(getApplicationContext(), rss_url).execute();

        }
        else {
            /** Start parsing xml data, if not find xml-content, the function will be called again soon */
            listViewLoaderTask.execute(xmlContent);
        }
    }
}