package yjhestem.ntnu.mobilewearprogramming.lab2;

import android.app.*;
import android.content.Intent;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */

public class GetWebsiteIntentService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String ACTION_FETCH_WEBSITE = "yjhestem.ntnu.mobilewearprogramming.lab2.action.FETCH_WEBSITE";
    private static final String ACTION_BAZ = "yjhestem.ntnu.mobilewearprogramming.lab2.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "yjhestem.ntnu.mobilewearprogramming.lab2.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "yjhestem.ntnu.mobilewearprogramming.lab2.extra.PARAM2";
    private static final String TAG = "GetWebsiteIntentService";
    public static final int SERVICE_ID = 5;
    //private boolean running = false;

    public GetWebsiteIntentService() {
        super("GetWebsiteIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static Intent startActionFetchWebsite(Context context, String param1, String param2) {
        Intent intent = new Intent(context, GetWebsiteIntentService.class);
        intent.setAction(ACTION_FETCH_WEBSITE);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
        return intent;
    }

    public static Intent startActionFetchWebsiteFromAlarmManager(Intent intent) {
        /*Intent intent = new Intent(context, GetWebsiteIntentService.class);
        intent.setAction(ACTION_FETCH_WEBSITE);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);*/
        return intent;
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, GetWebsiteIntentService.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FETCH_WEBSITE.equals(action)) {
                    final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                    final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                    handleActionFoo(param1, param2);
            } else if (ACTION_BAZ.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionBaz(param1, param2);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        new GetData(getApplicationContext(), param1).execute();
        //throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /*@Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: Changing running to false.");
        running = false;
        Log.d(TAG, "onDestroy: Changed running to false.");
    }*/
}
