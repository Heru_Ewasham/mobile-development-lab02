package yjhestem.ntnu.mobilewearprogramming.lab2;

//import android.preference.PreferenceActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.preference.PreferenceScreen;
//import android.widget.Button;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URISyntaxException;

import static android.content.Intent.URI_ANDROID_APP_SCHEME;
import static android.content.Intent.URI_INTENT_SCHEME;

public class SettingsActivity extends AppCompatActivity {

    String TAG = "SettingsActivity";
    Intent getWebsiteIntent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("SettingActivity", "onCreate: Starting activitysettings layout");
        setContentView(R.layout.activity_settings);
        //Log.d("SettingActivity", "onCreate: Getting preferences");
        //addPreferencesFromResource(R.xml.preferences);
        //Log.d("SettingActivity", "onCreate: Got preferences");

        //Log.d("SettingsActivity", "onCreate: Starting getFragmentManager()...");
        // Display the fragment as the main content.
        //getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String rssUrl = prefs.getString("RSS_URL", "http://www.nrk.no/toppsaker.rss");
        final String displayNumberValue = prefs.getString("displayNumber", "10");
        final String fetchTimeValue = prefs.getString("fetchTime", "10");

        EditText urlEditText = (EditText) findViewById(R.id.editText_url);
        urlEditText.setText(rssUrl, TextView.BufferType.EDITABLE);

        Spinner displayNumber=(Spinner) findViewById(R.id.spinnerDisplayNumber);
        Spinner fetchTime=(Spinner) findViewById(R.id.spinnerFetchTime);
        displayNumber.setSelection(getIndex(displayNumber, displayNumberValue));
        fetchTime.setSelection(getIndex(fetchTime, fetchTimeValue));
    }

    /**
     * This method is used to find the strings position in the spinner/dropdown.
     *
     * Gotten from: https://stackoverflow.com/questions/8769368/how-to-set-position-in-spinner
     */
    private int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }

        return index;
    }

    public void saveSettings(View view) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences.Editor editor = prefs.edit();

        /*Intent getWebsiteIntentService = null;
        String websiteIntentString = prefs.getString("getWebsiteIntent", null);
        Log.d(TAG, "saveSettings: Try to parse uri. Uri is: " + websiteIntentString);
        try {
            if (websiteIntentString != null) {
                getWebsiteIntentService = Intent.parseUri(websiteIntentString, URI_INTENT_SCHEME);
            }
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d(TAG, "saveSettings: Parsed uri.");
        if(getWebsiteIntentService != null){
            Log.d(TAG, "saveSettings: Stopping service");
            getApplicationContext().stopService(getWebsiteIntentService);
            Log.d(TAG, "saveSettings: Stopped service");
            getWebsiteIntentService = null;
        }*/
        EditText rss_URL = (EditText)findViewById(R.id.editText_url);
        Spinner displayNumber=(Spinner) findViewById(R.id.spinnerDisplayNumber);
        Spinner fetchTime=(Spinner) findViewById(R.id.spinnerFetchTime);

        /*String uriString = getWebsiteIntent.toUri(URI_INTENT_SCHEME);
        editor.putString("getWebsiteIntent", uriString);*/
        editor.putString("RSS_URL", rss_URL.getText().toString());
        editor.putString("displayNumber", displayNumber.getSelectedItem().toString());
        editor.putString("fetchTime", fetchTime.getSelectedItem().toString());
        editor.apply();

        Log.d(TAG, "saveSettings: Get xml from new url");
        new GetData(getApplicationContext(), rss_URL.getText().toString()).execute();

        Toast toast = Toast.makeText(getApplicationContext(), "Saved settings", Toast.LENGTH_SHORT);
        toast.show();
    }
}
