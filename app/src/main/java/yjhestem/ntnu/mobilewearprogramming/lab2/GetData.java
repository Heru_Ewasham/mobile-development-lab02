package yjhestem.ntnu.mobilewearprogramming.lab2;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Bruker on 08.02.2018.
 */

class GetData extends AsyncTask<String, Void, String> {
    Context context;
    //public Activity activity;
    public String website;
//.... other attributes

    public GetData(Context _context, String _url){

        this.context = _context.getApplicationContext();
        this.website = _url;
//other initializations...

    }


    @Override
    protected String doInBackground(String... params) {
        Log.d("lab2", "doInBackground: Starting..");
        HttpURLConnection urlConnection = null;
        String result = "";
        try {
            URL url = new URL(website);
            urlConnection = (HttpURLConnection) url.openConnection();

            int code = urlConnection.getResponseCode();

            if(code==200){
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                if (in != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                    String line = "";

                    while ((line = bufferedReader.readLine()) != null)
                        result += line;
                }
                in.close();
            }

            return result;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        finally {
            urlConnection.disconnect();
        }
        return result;

    }

    @Override
    protected void onPostExecute(String result) {
        Log.d("lab2", "onPostExecute: " + result);
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString("xmlData", result);
        editor.apply();

        super.onPostExecute(result);
    }
}