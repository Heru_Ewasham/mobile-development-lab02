package yjhestem.ntnu.mobilewearprogramming.lab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

//import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class WebActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        Log.d("lab2", "onCreateWeb: url gotten: " + url);
        setContentView(R.layout.activity_web);
        WebView webview=(WebView)findViewById(R.id.web);
        webview.setWebViewClient(new WebViewClient());
        // Simplest usage: note that an exception will NOT be thrown
        // if there is an error loading this page (see below).
        webview.loadUrl(url);
    }
}
