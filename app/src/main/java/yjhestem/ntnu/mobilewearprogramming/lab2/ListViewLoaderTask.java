package yjhestem.ntnu.mobilewearprogramming.lab2;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Bruker on 08.02.2018.
 */

public class ListViewLoaderTask extends AsyncTask<String, Void, SimpleAdapter> {

    public Activity activity;
    public int itemsToShow;
//.... other attributes

    public ListViewLoaderTask( Activity _activity, int _itemsToShow){

        this.activity = _activity;
        this.itemsToShow = _itemsToShow;
//other initializations...

    }


    /** Doing the parsing of xml data in a non-ui thread */
    @Override
    protected SimpleAdapter doInBackground(String... xmlData) {
        StringReader reader = new StringReader(xmlData[0]);
        //int itemsToShow = Integer.parseInt(xmlData[1]);
        XMLParser articleXmlParser = new XMLParser();

        List<HashMap<String, String>> articles = null;

        try{
            /** Getting the parsed data as a List construct */
            articles = articleXmlParser.parse(reader, itemsToShow);
        }catch(Exception e){
            Log.d("Exception",e.toString());
        }

        /** Keys used in Hashmap */
        String[] from = { "title","link","description"};

        /** Ids of views in listview_layout */
        int[] to = { R.id.title,R.id.link,R.id.description};

        /** Instantiating an adapter to store each items
         *  R.layout.listview_layout defines the layout of each item
         */
        SimpleAdapter adapter = new SimpleAdapter(activity.getBaseContext(), articles, R.layout.lv_layout, from, to);

        return adapter;
    }

    @Override
    protected void onPostExecute(SimpleAdapter adapter) {

        /** Getting a reference to listview of main.xml layout file */
        ListView listView = ( ListView ) activity.findViewById(R.id.List);

        /** Setting the adapter containing the country list to listview */
        listView.setAdapter(adapter);
    }
}
