package yjhestem.ntnu.mobilewearprogramming.lab2;

        import java.io.IOException;
        import java.io.Reader;
        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;

        import org.xmlpull.v1.XmlPullParser;
        import org.xmlpull.v1.XmlPullParserException;

        import android.util.Log;
        import android.util.Xml;

/**
 * Found code I then have edited here: http://wptrafficanalyzer.in/blog/android-xml-parsing-with-xmlpullparser-and-loading-to-listview-example/
 */
public class XMLParser {

    private static final String ns = null;

    /** This is the only function need to be called from outside the class */
    public List<HashMap<String, String>> parse(Reader reader, int itemsToShow)
            throws XmlPullParserException, IOException{
        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(reader);
            parser.nextTag();
            return readXML(parser, itemsToShow);
        }finally{
        }
    }

    /** This method read each item in the xml data and add it to List */
    private List<HashMap<String, String>> readXML(XmlPullParser parser, int itemsToShow)
            throws XmlPullParserException,IOException{

        List<HashMap<String, String>> list = new ArrayList<HashMap<String,String>>();

        parser.require(XmlPullParser.START_TAG, ns, "rss");

        while(parser.next() != XmlPullParser.END_TAG){
            if(parser.getEventType() != XmlPullParser.START_TAG){
                continue;
            }

            String name = parser.getName();

            // Check that the next tag is 'channel' (both NRK and VG (and most likely many others) has this format).
            if(name.equals("channel")){
                parser.require(XmlPullParser.START_TAG, ns, "channel");

                int antItems = 0;
                while(parser.next() != XmlPullParser.END_TAG){
                    if (antItems == itemsToShow) {
                        break;
                    }
                    if(parser.getEventType() != XmlPullParser.START_TAG){
                        continue;
                    }

                    name = parser.getName();
                    if(name.equals("item")){
                        antItems++;
                        Log.d("Lab2", "readXML: Number of items: " + antItems);

                        list.add(readItem(parser));
                    }
                    else{
                        skip(parser);
                    }
                }
            }
            else{
                skip(parser);
            }
        }
        return list;
    }

    /** This method read a country and returns its corresponding HashMap construct */
    private HashMap<String, String> readItem(XmlPullParser parser)
            throws XmlPullParserException, IOException{

        parser.require(XmlPullParser.START_TAG, ns, "item");

        /*String countryName = parser.getAttributeValue(ns, "name");
        String flag = parser.getAttributeValue(ns, "flag");*/
        String title="";
        String url="";
        String description="";
        //String currency="";

        while(parser.next() != XmlPullParser.END_TAG){
            if(parser.getEventType() != XmlPullParser.START_TAG){
                continue;
            }

            String name = parser.getName();

            if(name.equals("title")){
                title = readTitle(parser);
            }else if(name.equals("link")){
                //capital = parser.getAttributeValue(ns, "city");
                url = readURL(parser);
            }else if(name.equals("description")){
                //currencyCode = parser.getAttributeValue(ns, "code");
                description = readDescription(parser);
            }else{
                skip(parser);
            }
        }

        /*String details =    "Language : " + language + "\n" +
                "Capital : " + capital + "\n" +
                "Currency : " + currency + "(" + currencyCode + ")";*/

        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("title", title);
        hm.put("link", url);
        hm.put("description",description);

        Log.d("XMLParser", "readItem: Title = " + title);
        Log.d("XMLParser", "readItem: Link = " + url);
        Log.d("XMLParser", "readItem: Description = " + description);


        return hm;
    }

    /** Process language tag in the xml data */
    private String readTitle(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "title");
        String title = readText(parser);
        return title;
    }

    /** Process Capital tag in the xml data */
    private String readURL(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "link");
        String url = readText(parser);
        return url;
    }

    /** Process Currency tag in the xml data */
    private String readDescription(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "description");
        String description = readText(parser);
        return description;
    }

    /** Getting Text from an element */
    private String readText(XmlPullParser parser)
            throws IOException, XmlPullParserException{
        String result = "";
        if(parser.next()==XmlPullParser.TEXT){
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}